---
title: "TV相关"
date: 2020-12-12T18:23:32+08:00
draft: false
---



# Android_tv_libs

android tv 相关的库，包括界面开发，播放器等等的收集，

我并无法保证全部加入，必定会遗漏一些优秀的TV相关的库，

欢迎大家一起往里面添砖加瓦.



TV开发 QQ群1：522186932 QQ群2:468357191


android tv文章专题：https://www.jianshu.com/c/3f0ab61a1322

我的个人博客：https://blog.csdn.net/qw85525006/category_6228458.html [欢迎一起学习]

玩转Android，每日优质文章推荐：https://www.wanandroid.com/

[网站导航](https://gitee.com/kumei/Android_tv_libs/blob/master/%E7%BD%91%E9%A1%B5%E5%AF%BC%E8%88%AA.md)



[TOC]


# TV开发相关控件/框架/文章



## TV开发 Leanback 相关

### 谷歌 leanback demo

项目地址：https://github.com/googlesamples/leanback-showcase

谷歌原生的 leanback demo，看完此demo，基本就懂如何使用了.

leanback很灵活，主要是MVP的思想.

![](https://images.gitee.com/uploads/images/2019/1118/174701_017670f8_111902.png)



### V14Leanback 

项目地址：https://github.com/DukerSunny/V14Leanback

该项目主要是将leanback代码复制出来了.

一个适用于Android TV端的分页加载列表库，控件继承自RecyclerView，部分源码抽取自Google Support v17 Leanback包下源码，可兼容低版本环境。



### Leanback修改代码

项目地址：https://github.com/FrozenFreeFall/Leanback_tv_widget

项目只修改了一些代码.

![](https://images.gitee.com/uploads/images/2019/0619/151234_a7cbb98f_111902.png)


### BrowseFragment 自定义

项目地址：https://github.com/dextorer/Sofa

A library for Android TV that extends the Leanback library functionalities

对 Leanback 库的BrowseFragment 进行自定义，让一个 Header对应一个页面。不过最新 Leanback 库已经支持该功能。

![](https://images.gitee.com/uploads/images/2019/0522/165936_87fdc511_111902.jpeg)










---



## TV开发 RecyclerView 相关



### CustomTvRecyclerView 

项目地址：https://github.com/songwenju/CustomTvRecyclerView

该项目实现了 RecyclerView 按键翻页效果，可以参考代码并使用.

![](https://github.com/songwenju/CustomTvRecyclerView/raw/master/raw/master/screenshots/tvRecycler2.0.gif)



### TVRecyclerView

项目地址：https://github.com/Fredlxy/TVRecyclerView

封装过的RecyclerView，用于TV开发，



### TvRecyclerView--针对TV端特性进行的适配与开发

项目地址：https://github.com/zhousuqiang/TvRecyclerView

此项目在TwoWayView的基础上进行的改进，支持焦点快速移动，支持Item选中放大时不被叠压(无需手动调用bringChildToFront())，支持横/竖排列，支持设置选中Item边缘距离/居中，支持设置横竖间距，Item监听回调

![](https://github.com/zhousuqiang/TvRecyclerView/raw/master/images/img_all.gif)



### TvRecyclerView

项目地址：https://github.com/henryblue/TvRecyclerView 

用在android TV端的自定义recyclerView控件, 不支持手机端.

![](https://images.gitee.com/uploads/images/2019/0522/165933_1357ab48_111902.png)



### VLayout

项目地址：https://github.com/FrozenFreeFall/vlayout

这么说的，虽然只是移动端，并不支持TV端，但是此控件的思想不错，也避免了leanback在低配机器耗性能的问题(Recyclerview嵌套)，修改支持TV端，是一个神器.

![](https://images.gitee.com/uploads/images/2019/1014/160007_5a3f8001_111902.gif)

### tangram

项目地址：https://github.com/alibaba/tangram-android

项目相关文章：https://mp.weixin.qq.com/s/S1yP_RSgDfqFSbuDImrpuw

使用JSON的数据格式来加载界面，很有参考和学习的代码.



### TwoWayView

项目地址：https://github.com/lucasr/twoway-view

此项目可以配合leanback使用，嵌入1 N 布局，需要修改leanback源码.

![](https://images.gitee.com/uploads/images/2019/0522/165931_a66cb1ec_111902.png)


---



## TV开发框架



### Android tv metro

项目地址：https://gitee.com/kumei/android_tv_metro

此项目由开源社区修改，可编译版本，并添加了代码分析注释，原始源码自行下载.

在此重点说明下：tv metro的架构思想和部分代码是很有参考价值的，布局是动态生成的.



### Android tv frame

项目地址：https://gitee.com/kumei/AndroidTVWidget

最新项目重构地址：https://gitee.com/kumei/android-tv-frame-new

此项目包含了 移动边框，键盘，listview, gridview, recylerview等等

![](http://git.oschina.net/uploads/images/2016/0406/110716_e9f61513_111902.png)





### tv widget

项目地址：https://github.com/evilbinary/TvWidget

此项目可以库使用，也可以作为代码学习，还是不错.

![](https://images.gitee.com/uploads/images/2019/0601/005424_84694f54_111902.png)





### 移动边框BorderViewDemo

项目地址：https://github.com/lf8289/BorderViewDemo



### Android Tv 焦点框框架
支持设置颜色或图片作为焦点框; 支持圆角变化

项目地址：https://github.com/zhousuqiang/TvFocusBorder

![](https://images.gitee.com/uploads/images/2019/1118/174849_d467789f_111902.gif)



### Android Tv Widget Demo

项目地址: https://github.com/zhousuqiang/TvWidget

![](https://images.gitee.com/uploads/images/2019/1130/023736_53840945_111902.gif)



### fultter tv 开发框架

项目地址：https://github.com/coderJohnZhang/flutter_tv

https://github.com/flutter/flutter/issues/13749

https://github.com/flutter/flutter/issues/13633




---



## Launcher参考源码



### LeanbackTvSample

文章中讲解了 ViewPager + Leanback的使用，也完成了大部分的DEMO，可以参考图片

项目地址：https://github.com/iSuperRed/LeanbackTvSample

文章地址：https://www.jianshu.com/p/b286e087b074

![](https://upload-images.jianshu.io/upload_images/1893386-ca4cd630f57c93a8.gif?imageMogr2/auto-orient/strip|imageView2/2/w/640/format/webp)





### SMTVLauncher

项目地址：https://github.com/FrozenFreeFall/SMTVLauncher

![](https://images.gitee.com/uploads/images/2019/0601/005424_0fdce2ec_111902.jpeg)





### AndroidTVLauncher

项目地址：https://github.com/JackyAndroid/AndroidTVLauncher

此项目基于 Leanback，可以参考参考别人使用leanback的代码.

![](https://images.gitee.com/uploads/images/2019/1014/160007_0c80fc8d_111902.png)



### TVSample

项目地址：https://github.com/hejunlin2013/TVSample

两个 TV Launcher 页面例子：

1、仿泰捷视频最新TV版 Metro UI。

2.仿腾讯视频 TV 版(云视听·极光) 列表页(用 RecycleView + GridLayoutManager 实现)

![](https://github.com/hejunlin2013/TVSample/raw/master/images/device-2016-10-13-191954.png）



### Leanback桌面demo

项目地址: https://gitee.com/chuangshiji/Launcher




---



## 适配库



### AndroidAutoSize

项目地址：https://github.com/JessYanCoding/AndroidAutoSize



### AutoLayout

项目地址：https://github.com/hongyangAndroid/AndroidAutoLayout

文章: https://blog.csdn.net/lmj623565791/article/details/49990941

作者暂时不维护了，为了支持没有的控件或者未知函数，需要改一些东西.

使用的很简单，添加设计图分辨率，使用PX... ...



### SupportMultipleScreensUtil

项目地址：https://github.com/baixiaoY/SupportMultipleScreensUtil

调用方便，只需要调用它的转换函数就OK了.



### AutoUtils

项目地址: https://gitee.com/kumei/android-tv-frame-new/blob/develop/tv-widget/common/src/main/java/com/open/common/AutoUtils.java

手动写适配，简单易用



### 工具生成values

工具地址：https://gitee.com/kumei/AndroidTVWidget/tree/master/Tool

这种方式不建议使用了，新出的机型分辨率需要重新生成，pass吧.



### ConstraintLayout

Android新出的约束，也可以用于部分适配，还是不错，也能优化界面的布局，避免嵌套太深.




---



## 动画库



### 设计师与工程师 动效 协作 工具

项目地址：https://gitee.com/hailongqiu/directTool

![输入图片说明](https://images.gitee.com/uploads/images/2020/0425/151512_5b0a6012_111902.gif "f4vvvnB8iI.gif")





### [AndroidViewAnimations](https://links.jianshu.com/go?to=https%3A%2F%2Fgithub.com%2Fdaimajia%2FAndroidViewAnimations) 动画合集



### 属性动画封装后 OpenAnim

项目地址: https://gitee.com/kumei/OpenAnim



### SVG动画

项目地址: https://github.com/alexjlockwood/adp-delightful-details

![](https://images.gitee.com/uploads/images/2019/0601/005439_050a807c_111902.gif)

项目地址：https://github.com/Pixplicity/sharp



### recyclerview-animators 动画库

项目地址: https://github.com/wasabeef/recyclerview-animators



### [Material-Animations](https://github.com/lgvalle/Material-Animations)

No maintainance is intended. The content is still valid as a reference but it won't contain the latest new stuff



### [Loop3DRotation-master](https://links.jianshu.com/go?to=https%3A%2F%2Fgithub.com%2Fshenpeibao%2FLoop3DRotation-master) 3D旋转动画



### [ParticleTextView](https://links.jianshu.com/go?to=https%3A%2F%2Fgithub.com%2FYasic%2FParticleTextView) 粒子动画



### [TextSurface](https://links.jianshu.com/go?to=https%3A%2F%2Fgithub.com%2Felevenetc%2FTextSurface) 炫酷的文字展示动画





---



## 其它View相关



### 阴影控件 

项目地址：https://github.com/yingLanNull/ShadowImageView

![](https://images.gitee.com/uploads/images/2019/0601/005434_288a4a3b_111902.png)



### 简单的阴影控件

项目地址：https://github.com/lijiankun24/ShadowLayout

![](https://images.gitee.com/uploads/images/2019/0619/151255_145ddfe3_111902.png)



### 角标控件

项目地址: https://github.com/H07000223/FlycoLabelView，https://github.com/linger1216/labelview

![](http://git.oschina.net/uploads/images/2016/0301/115509_4dfe34ba_111902.png)



### 圆角控件

项目地址: https://github.com/Y-bao/RoundAngleFrameLayout

![输入图片说明](https://images.gitee.com/uploads/images/2018/1031/002556_4cbe9ff8_111902.png "圆角控件")



### 星级控件

项目地址: https://github.com/DreaminginCodeZH/MaterialRatingBar

![](https://images.gitee.com/uploads/images/2018/1101/162250_0ba661bb_111902.png "")



### 轮播 [BannerViewPager](https://links.jianshu.com/go?to=https%3A%2F%2Fgithub.com%2Fzhpanvip%2FBannerViewPager)

腾讯视频、QQ音乐、酷狗音乐、支付宝、天猫、淘宝、优酷视频、喜马拉雅、网易云音乐、哔哩哔哩、全民K歌等App的Banner样式都可以通过BannerViewPager实现哦！



### 轮播 [banner](https://links.jianshu.com/go?to=https%3A%2F%2Fgithub.com%2Fyouth5201314%2Fbanner)

![https://github.com/youth5201314/banner/blob/master/images/banner_example.gif](https://github.com/youth5201314/banner/blob/master/images/banner_example.gif?raw=true)



### Litho UI库

Litho是Facebook推出的一套高效构建Android UI的声明式框架，主要目的是提升RecyclerView复杂列表的滑动性能和降低内存占用。

[Litho官网](https://fblitho.com/)

[美团文章推荐](https://tech.meituan.com/2019/03/14/litho-use-and-principle-analysis.html)





### 主题切换

项目地址：https://github.com/fengjundev/Android-Skin-Loader 使用APK资源resource加载流

项目地址：https://github.com/ximsfei/Android-skin-support Resource替换流的解决方案

项目地址：https://github.com/qqliu10u/QSkinLoader QSkinLoader换肤框架

项目地址：https://github.com/hehonghui/Colorful Colorful



### 颜色选择器

项目地址：https://github.com/LarsWerkman/HoloColorPicker HoloColorPicker

项目地址：https://github.com/attenzione/android-ColorPickerPreference  android-ColorPickerPreference






---



## 相关文章



### Leanback

[AndroidTv Home界面实现原理（一）——Leanback 库的使用](https://www.jianshu.com/p/e48b4db2b07d)
[聊一聊 Leanback 中的 HorizontalGridView](https://www.jianshu.com/p/52f67a414a10)



### RecyclerView

RecyclerView DiffUtil



### 焦点

[Android TV开发-按键焦点了解与应用](https://blog.csdn.net/qw85525006/article/details/84577313)




## 播放器



### UPYUN Android 流媒体播放器

项目地址:https://github.com/upyun/android-player-sdk

android-player-sdk 是一个适用于 Android 平台的影音播发器 SDK ，基于 ijkplayer ( based on ffplay )，可高速定制化和二次开发，为 Android 开发者提供简单，快捷的接口。



### GSYVideoPlayer

项目地址：https://github.com/CarGuo/GSYVideoPlayer

视频播放器（IJKplayer），HTTPS支持，支持弹幕，支持滤镜、水印、gif截图，片头广告、中间广告，多个同时播放，支持基本的拖动，声音、亮度调节，支持边播边缓存，支持视频本身自带rotation的旋转（90,270之类），重力旋转与手动旋转的同步支持，支持列表播放 ，直接添加控件为封面，列表全屏动画，视频加载速度，列表小窗口支持拖动，5.0的过场效果，调整比例，多分辨率切换，支持切换播放器，进度条小窗口预览，其他一些小动画效果，rtsp、concat、mpeg。简书:<http://www.jianshu.com/p/9fe377dd9750>

![](https://github.com/CarGuo/GSYVideoPlayer/raw/master/33.gif)



### ExoPlayer

项目地址：https://github.com/google/ExoPlayer/



### vitamio 

官网地址：https://www.vitamio.org/ 个人免费，商用收费，请注意.

项目地址：https://github.com/yixia/VitamioBundle



### [VideoPlayerManager](https://links.jianshu.com/go?to=https%3A%2F%2Fgithub.com%2Fdanylovolokh%2FVideoPlayerManager) 

帮助实现VideoPlayer控制，使得它更容易使用ListView和recyclerview。

This is a project designed to help controlling Android MediaPlayer class. It makes it easier to use MediaPlayer ListView and RecyclerView. Also it tracks the most visible item in scrolling list. When new item in the list become the most visible, this library gives and API to track it.



### [PlayerBase](https://links.jianshu.com/go?to=https%3A%2F%2Fgithub.com%2Fjiajunhui%2FPlayerBase)

**PlayerBase**是一种将解码器和播放视图组件化处理的解决方案框架。您需要什么解码器实现抽象引入即可，对于视图，无论是播放器内的控制视图还是业务视图，均可以做到组件化处理。将播放器的开发变得清晰简单，更利于产品的迭代。

**PlayerBase**不会为您做任何多余的功能业务组件，有别于大部分播放器封装库的通过配置或者继承然后重写然后定制你需要的功能组件和屏蔽你不需要的功能组件（low!!!）。正确的方向应该是需要什么组件就拓展添加什么组件，而不是已经提供了该组件去选择用不用。



### [JiaoZiVideoPlayer](https://links.jianshu.com/go?to=https%3A%2F%2Fgithub.com%2Flipangit%2FJiaoZiVideoPlayer) 饺子视频播放器

项目地址：https://github.com/lipangit/JiaoZiVideoPlayer，https://github.com/Jzvd/JZVideo



### [ijkplayer](https://links.jianshu.com/go?to=https%3A%2F%2Fgithub.com%2Fbilibili%2Fijkplayer) ijk 播放器

项目地址：https://github.com/bilibili/ijkplayer



### [DanmakuFlameMaster](https://links.jianshu.com/go?to=https%3A%2F%2Fgithub.com%2Fbilibili%2FDanmakuFlameMaster) 弹幕

android上开源弹幕解析绘制引擎项目；使用多种方式(View/SurfaceView/TextureView)实现高效绘制；B站xml弹幕格式解析；支持多种显示效果选项实时切换；换行弹幕支持/运动弹幕支持；支持自定义字体；等等



### [AndroidVideoCache](https://links.jianshu.com/go?to=https%3A%2F%2Fgithub.com%2Fdanikula%2FAndroidVideoCache) 视频缓存

Because there is no sense to download video a lot of times while streaming! `AndroidVideoCache` allows to add caching support to your `VideoView/MediaPlayer`, [ExoPlayer](https://github.com/danikula/AndroidVideoCache/tree/exoPlayer) or any another player with help of single line!



### [NiceVieoPlayer](https://links.jianshu.com/go?to=https%3A%2F%2Fgithub.com%2Fxiaoyanger0825%2FNiceVieoPlayer) 支持列表，完美切换全屏、小窗口的Android视频播放器

用IjkPlayer/MediaPlayer + TextureView封装，可切换IjkPlayer、MediaPlayer；支持本地和网络视频播放； 完美切换小窗口、全屏，可在RecyclerView中无缝全屏；手势滑动调节播放进度、亮度、声音；支持清晰度切换；可自定义控制界面.





## TV输入法



### 台湾注音输入法，使用注音引擎，可替换拼音或者其它的引擎.

项目地址: https://gitee.com/kumei/xgimi_zhuyin_input 



### 原生的谷歌输入法，可单独编译版本.

项目地址:https://gitee.com/kumei/PinyinIME_GOOGLE 



### 英文输入法.高仿搜狗界面.

项目地址:https://gitee.com/kumei/OpenInputMethod 





# 其它



## 缓存库 DiskLruCache

项目地址：https://github.com/JakeWharton/DiskLruCache



## 热修复/插件化/AOP



### [tinker](https://links.jianshu.com/go?to=https%3A%2F%2Fgithub.com%2FTencent%2Ftinker) 它支持dex、库和资源更新，无需重新安装apk



### [Bugly Android热更新](https://links.jianshu.com/go?to=https%3A%2F%2Fbugly.qq.com%2Fdocs%2Fuser-guide%2Finstruction-manual-android-hotfix%2F%3Fv%3D20200312155538)它封装了tinker是腾讯团队维护的，完全免费！



### [AndFix](https://links.jianshu.com/go?to=https%3A%2F%2Fgithub.com%2Falibaba%2FAndFix) 阿里的方案



### [VirtualXposed](https://github.com/android-hacker/VirtualXposed) 是基于VirtualApp 和 epic 在非ROOT环境下运行Xposed模块的实现



### [Nuwa](https://links.jianshu.com/go?to=https%3A%2F%2Fgithub.com%2Fjasonross%2FNuwa)

Nuwa is a goddess in ancient Chinese mythology best known for repairing the pillar of heaven.

With this Nuwa project，you can also have the repairing power, fix your android applicaiton without have to publish a new APK to the appstore.



### AOP轻量级框架

项目地址：https://github.com/eleme/lancet



## AdbLib

项目地址：https://github.com/cgutman/AdbLib



## 反射库

项目地址：https://github.com/jOOQ/jOOR

很方便的使用反射函数，简单，高效，一行代码搞定反射调用，不用写很多代码



## 权限管理SDK



### [AndPermission](https://github.com/yanzhenjie/AndPermission)

官方网站:http://yanzhenjie.github.io/AndPermission/cn




## HDMI DEMO

项目地址：https://gitee.com/kumei/TIFSample

博客地址：http://www.jianshu.com/p/385c92fceb16



## zxing 二维码

项目地址：https://github.com/zxing/zxing




# 网络相关


## 网络框架

### RxHttp

项目地址：https://github.com/liujingxing/RxHttp

### OkGo，一个专注于让网络请求更简单的框架，与RxJava完美结合，比Retrofit更简单易用。

项目地址：https://github.com/jeasonlzy/okhttp-OkGo

文章地址：https://www.jianshu.com/p/6aa5cb272514


---


## 跨进程通信


### 腾讯Hardcoder Android通讯框架

项目地址：https://github.com/Tencent/Hardcoder

文章解读：https://segmentfault.com/a/1190000025180511



### 进程间通信库Hermes

项目地址：https://github.com/Xiaofei-it/Hermes-IPC-Demo

项目地址：https://github.com/Xiaofei-it/Hermes

项目地址：https://github.com/elemers/HermesEventBus 跨进程通信的eventbus



### 基于JSON RPC的一种Android跨进程调用

文章解读：https://www.jianshu.com/p/1eca5e32fad2?hmsr=toutiao.io&utm_medium=toutiao.io&utm_source=toutiao.io

项目地址：https://github.com/iqiyi/Andromeda


## DroidDLNA相关

项目地址：https://github.com/offbye/DroidDLNA  https://github.com/4thline/cling



## MiniThunder 

android迷你版迅雷，支持thunder:// ftp:// http:// ed2k:// 磁力链 种子文件的下载，音视频文件支持边下边播.

项目地址：https://gitee.com/kumei/MiniThunder



## nanohttpd 轻量级的 HTTP server

项目地址：https://github.com/NanoHttpd/nanohttpd



## 远程控制你的智能电视，按键|输入|安装App等都已实现.

项目地址：https://github.com/kingthy/TVRemoteIME

文章地址：https://juejin.im/post/5a9e47636fb9a028c42db81d







## 投屏SDK  ConnectSDK，在其中对大部分协议做了支持。

To communicate with discovered devices, Connect SDK integrates support for protocols such as  **DLNA** ,  **DIAL** ,  **SSAP** ,  **ECG** ,  **AirPlay** ,  **Chromecast** ,  **UDAP** , and webOS second screen protocol. Connect SDK intelligently picks which protocol to use depending on the feature being used.

官网：connectsdk.com/

Github：github.com/connectsdk



# 工具



## 运营类相关：

诸葛IO：https://zhugeio.com/

神策数据：https://www.sensorsdata.cn/auto



## 研发体系相关：

Bmob 后端云，全方位一体化的后端服务平台：https://www.bmob.cn/

蒲公英：https://www.pgyer.com/

蓝湖，高效 的产品，设计，研发 协作平台:https://www.lanhuapp.com/

PerfDog性能测试分析平台：https://perfdog.qq.com/

代码托管平台，码云：https://gitee.com/

一站式 DevOps 提升研发效能：https://coding.net/

Github代码托管:https://github.com/



## 其它



## 阴影生成工具: https://inloop.github.io/shadow4android/



## 思考框架图绘制工具：http://boomar.cn/  比如 时间管理，SWOT分析，5W2H分析，复盘等等



## [Apktool](https://links.jianshu.com/go?to=https%3A%2F%2Fgithub.com%2FiBotPeaches%2FApktool) 反编译工具



# 推荐一些不错的Android书籍

## 专业书籍

《Android自定义控件开发入门与实战》，《App研发录：架构设计、Crash分析和竞品技术分析》，《Android Launcher应用开发》,《Android开发艺术探索》

《Android插件化开发指南》，《Android组件化架构》，《深入探索Android热修复技术原理》,《Android音视频开发》,《揭秘Kotlin编程原理》

《Android 源码设计模式解析与实战》，《移动开发架构设计实战》

《移动APP性能评测与优化》，《Android移动性能实战》

《精通Android Studio/移动开发丛书》，《Android Gradle权威指南》

## 软技能

《程序员的成长课》，《解忧程序员》







# 参考资料

[最新最全Android 常用开源库总结](https://www.jianshu.com/p/3fde87405411)

